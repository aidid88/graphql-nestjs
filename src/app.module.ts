import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';
import { GraphQLModule } from '@nestjs/graphql';
import { TypeOrmModule } from '@nestjs/typeorm';
import { graphqlUploadExpress } from 'graphql-upload';
import { join } from 'path';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { LottieModule } from './lottie/lottie.module';

@Module({
  imports: [
    GraphQLModule.forRoot({
      autoSchemaFile: join(process.cwd(), 'src/schema.gql'),
      path: '/graphql',
      uploads: false,
    }),
    TypeOrmModule.forRoot({
      type: 'postgres',
      database: 'lottie',
      host: 'localhost',
      port: 5432,
      username: 'root',
      password: 'root',
      entities: ['dist/**/*.entity{.ts,.js}'],
      synchronize: true,
    }),
    LottieModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule  implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(graphqlUploadExpress()).forRoutes('graphql');
  }
}
