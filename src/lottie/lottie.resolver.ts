import { Resolver, Query, Mutation, Args, Int } from '@nestjs/graphql';
import { GraphQLUpload, FileUpload } from 'graphql-upload';
import { createWriteStream } from 'fs';
import { CreateLottieInput } from './dto/create-lottie.input';
import { Lottie } from './lottie.entity';
import { LottieService } from './lottie.service';
import { UploadLottie } from './dto/upload-lottie-ouput';

@Resolver((of) => Lottie)
export class LottieResolver {
  constructor(private lottieService: LottieService) {}

  @Query((returns) => Lottie)
  getLottie(@Args('id', { type: () => Int }) id: number): Promise<Lottie> {
    return this.lottieService.findOne(id);
  }

  @Query((returns) => [Lottie])
  lotties(): Promise<Lottie[]> {
    return this.lottieService.findAll();
  }

  @Mutation((returns) => Lottie)
  createLottie(
    @Args('createLottieInput') createLottieInput: CreateLottieInput,
  ): Promise<Lottie> {
    return this.lottieService.createLottie(createLottieInput);
  }

  @Mutation((returns) => String)
  async uploadFile(
    @Args({ name: 'file', type: () => GraphQLUpload })
    { createReadStream, filename }: FileUpload,
  ): Promise<any> {
    return new Promise(async (resolve, reject) =>
      createReadStream()
        .pipe(createWriteStream(`./uploads/${filename}`))
        .on('finish', () => resolve(filename))
        .on('error', () => reject('')),
    );
  }
}
