import { Test, TestingModule } from '@nestjs/testing';
import { LottieResolver } from './lottie.resolver';

describe('LottieResolver', () => {
  let resolver: LottieResolver;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [LottieResolver],
    }).compile();

    resolver = module.get<LottieResolver>(LottieResolver);
  });

  it('should be defined', () => {
    expect(resolver).toBeDefined();
  });
});
