import { Field, InputType } from '@nestjs/graphql';
import { IsAlpha } from 'class-validator';

@InputType()
export class CreateLottieInput {
  @Field()
  filePath: string;

  @IsAlpha()
  @Field()
  name: string;

  @Field()
  description: string;

  @IsAlpha()
  @Field()
  author: string;
}
