import { Field, ObjectType } from '@nestjs/graphql';

@ObjectType()
export class UploadLottie {
  @Field()
  filePath: string;
}
