import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateLottieInput } from './dto/create-lottie.input';
import { Lottie } from './lottie.entity';

@Injectable()
export class LottieService {
  constructor(
    @InjectRepository(Lottie) private lottieRepository: Repository<Lottie>,
  ) {}

  async createLottie(createLottieInput: CreateLottieInput): Promise<Lottie> {
    const newLottie = await this.lottieRepository.create(createLottieInput);
    return this.lottieRepository.save(newLottie);
  }

  async findAll(): Promise<Lottie[]> {
    return this.lottieRepository.find();
  }

  async findOne(id: number): Promise<Lottie> {
    return this.lottieRepository.findOneOrFail(id);
  }
}
