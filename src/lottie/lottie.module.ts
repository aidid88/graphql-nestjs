import { Module } from '@nestjs/common';
import { LottieService } from './lottie.service';
import { LottieResolver } from './lottie.resolver';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Lottie } from './lottie.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Lottie])],
  providers: [LottieService, LottieResolver],
})
export class LottieModule {}
