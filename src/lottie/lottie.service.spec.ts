import { Test, TestingModule } from '@nestjs/testing';
import { LottieService } from './lottie.service';

describe('LottieService', () => {
  let service: LottieService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [LottieService],
    }).compile();

    service = module.get<LottieService>(LottieService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
